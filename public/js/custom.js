var REQUEST_ENCODE = '#reqEncode';
var REQUEST_DECODE = '#reqDecode';
var MD5_ENCODE = 'encodeStr';
var MD5_DECODE = 'decodeStr';
var ENCODE_DECODE_FIELD = '#encodeDecode';
var RESULT = '#result';

$(REQUEST_ENCODE).click(function() {
    $.get(MD5_ENCODE, { key: $(ENCODE_DECODE_FIELD).val() }, function(res) {
        $(RESULT).text(res);
    });
    return false;
});

$(REQUEST_DECODE).click(function() {
    $.get(MD5_DECODE, { key: $(ENCODE_DECODE_FIELD).val() }, function(res) {
        if(res == '') {
            $(RESULT).text('I can not decode');
        } else {
            $(RESULT).text(res);
        }
    });
    return false;
});
