<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Hash;

class HashController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }

    public function encodeStr(Request $request, Hash $hash)
    {
        $getStr = $request->all();
        $hashStr = $hash->encode($getStr['key']);
        return $hashStr;
    }

    public function decodeStr(Request $request, Hash $hash)
    {
        $getHash = $request->all();
        $decodeStr = $hash->decode($getHash['key']);
        return $decodeStr;
    }
}
