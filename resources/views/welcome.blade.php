<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>

    </head>
    <body>
    <div class="flex-center position-ref full-height">
    @if (Route::has('login'))
            <div class="top-right links">
                <a href="{{ url('/login') }}">Login</a>
                <a href="{{ url('/register') }}">Register</a>
            </div>
        @endif
        <div class="content">
            <h1 class="title m-b-md">
                Hashing MD5
            </h1>

            <div class="wrap-form">
                <form action="" method="get" class="form-hash" id="formHash">
                    <input type="text" class="form-hash__encode-decode" id="encodeDecode">
                    <div class="form-hash__result" id="result">Select an action</div>
                    <div>
                        <button class="btn btn-primary" id="reqEncode">Encode</button>
                        <button class="btn btn-default" id="reqDecode">Decode</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/custom.js') }}"></script>
    </body>
</html>
